# ETL Data

## Overview
This Jupyter notebook generates an interactive map of ETL members - hover over each sector to see the count of members in each sector.

## Methodology
- Extracts membership data from myTurn
- Filter out non-verified members
- Cleanse the Postal Codes (ensure uppercase)
- Enrich with Postal Sectors (e.g. "EH16 5XX" -> "EH16 5")
- Count number of members by Postal Sector
- Plots the member count per sector on a choropleth map

Records that don't have a valid postal sector are reported separately, for data quality correction purposes.

## Converting to HTML
To generate HTML:
````
jupyter nbconvert --no-input membermap.ipynb --to html
````
